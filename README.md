 # Quantum Antivirus Ethereal (By Freedomdao/Graylan)
### Installation Guide:
 **Install Anaconda**:
   - Download the Anaconda installer for your operating system from the [official Anaconda website](https://www.anaconda.com/products/distribution).
   - Follow the installation instructions provided on the Anaconda website for your specific operating system.
   - After installation, make sure Anaconda is added to your system's PATH variable. You can typically do this by selecting the option during installation or by manually adding Anaconda's `bin` directory to the PATH.To install and run the "Advanced Antivirus System" app, follow these steps:
Sure, here's an updated installation guide with instructions on how to install Anaconda:


 **Install Required Python Packages using Anaconda**:
   - Open a terminal or command prompt.
   - Create a new Anaconda environment (optional but recommended):
     ```bash
     conda create --name advanced-antivirus python=3.11
     ```
     Replace `advanced-antivirus` with the desired name for your environment.
   - Activate the created environment:
     - **On
       ```bash
       conda activate advanced-antivirus
       ```

### Installation Guide:
Install the required Python packages using pip:
   ```bash
   pip install tkinter asyncio aiosqlite bleach cryptography customtkinter llama-cpp
   ```

### List of Required Python Packages:
- tkinter: For building the graphical user interface (GUI).
- asyncio: For handling asynchronous tasks.
- aiosqlite: For asynchronous SQLite database operations.
- bleach: For sanitizing input data before storing it in the database.
- cryptography: For encryption and decryption of data.
- customtkinter: Custom tkinter widgets for enhanced UI elements.
- llama-cpp: Python interface to the Llama AI model.

### Guide to Download Llama Model:
You can download the Llama model from the Hugging Face Model Hub. Here are the steps:
1. Visit the model page: [Llama-2-7B-Chat-GGML](https://huggingface.co/TheBloke/Llama-2-7B-Chat-GGML/blob/main/llama-2-7b-chat.ggmlv3.q8_0.bin).
2. Click on the "Download" button to download the model file and save the model to the directory containing the script (Same on windows and linux and mac) (`llama-2-7b-chat.ggmlv3.q8_0.bin`).


### Running the Application:
 Once you've installed Anaconda and downloaded the Llama model, navigate to the directory containing your Python script.
 Activate the Anaconda environment created earlier (if you created one):
   ```bash
   conda activate advanced-antivirus
   ```
 Run the Python script containing the app code:
   ```bash
   python qantivirus.py
   ```