import time
import tkinter as tk
import asyncio
import logging
import aiosqlite
import threading
import bleach
import customtkinter as ctk
from llama_cpp import Llama
from cryptography.fernet import Fernet
import concurrent.futures
import re
import psutil
import numpy as np
import pennylane as qml

class App(ctk.CTk):
    def __init__(self):
        super().__init__()
        self.title("Advanced Antivirus System")
        self.geometry("1100x580")

        self.setup_logging()

        self.automatic_prompt_var = tk.BooleanVar()
        self.automatic_prompt_switch = ctk.CTkSwitch(self, text="Activate Automatic Prompt", variable=self.automatic_prompt_var, command=self.toggle_automatic_prompt)
        self.automatic_prompt_switch.grid(row=0, column=0, padx=20, pady=(20, 10), sticky="w")

        self.llama2_output_text = ctk.CTkTextbox(self, height=205, width=601, font=("Arial", 12))
        self.llama2_output_text.grid(row=1, column=0, padx=20, pady=10, sticky="w")

        self.color_indicator_canvas = ctk.CTkCanvas(self, width=40, height=40)
        self.color_indicator_canvas.grid(row=2, column=0, padx=20, pady=10, sticky="w")

        self.automatic_prompt_thread = None
        self.automatic_prompt_interval = 45 * 60

        self.llm = None
        self.db_name = "llama_database.db"
        self.fernet_key = Fernet.generate_key()
        self.fernet = Fernet(self.fernet_key)

        self.thread_pool_executor = concurrent.futures.ThreadPoolExecutor(max_workers=1)

        self.load_model_and_start_gui()

    def setup_logging(self):
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
        fh = logging.FileHandler('app.log')
        fh.setLevel(logging.INFO)
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)

    def load_model_and_start_gui(self):
        try:
            self.load_model()
            self.setup_database()
            self.start_gui()
        except Exception as e:
            self.logger.error(f"Error loading the model: {e}")
            self.start_gui()

    def start_gui(self):
        self.logger.info("Starting GUI")
        self.mainloop()

    def load_model(self):
        model_name = "llama-2-7b-chat.ggmlv3.q8_0.bin"
        self.logger.info(f"Loading model: {model_name}")
        self.llm = Llama(
            model_path=model_name,
            n_gpu_layers=-1,
            n_ctx=3900,
        )
        self.logger.info("Model loaded successfully")

    def toggle_automatic_prompt(self):
        if self.automatic_prompt_var.get():
            self.start_automatic_prompt()
        else:
            self.stop_automatic_prompt()

    def start_automatic_prompt(self):
        self.logger.info("Starting automatic prompt")
        self.automatic_prompt_thread = threading.Thread(target=self.automatic_prompt_worker)
        self.automatic_prompt_thread.daemon = True
        self.automatic_prompt_thread.start()

    def stop_automatic_prompt(self):
        if self.automatic_prompt_thread:
            self.logger.info("Stopping automatic prompt")
            self.automatic_prompt_thread.join()
            self.automatic_prompt_thread = None

    def automatic_prompt_worker(self):
        self.logger.info("Automatic prompt worker started")
        while True:
            self.generate_automatic_prompt()
            time.sleep(self.automatic_prompt_interval)

    def generate_automatic_prompt(self):
        self.logger.info("Generating automatic prompt")
        retries = 3  # Number of retries
        while retries > 0:
            quantum_state = self.offline_quantum_circuit_analysis(self.get_ram_usage_without_internet(), self.get_cpu_usage())
            automatic_prompt = f"""[pastcontext]Last prompt generated successfully. System performing Quantum RAM and CPU analysis ({quantum_state}).[/pastcontext]
[action: Execute] [task: Initiate Ethereal Hack Checker AI System Scan] [request: Verify Machine and Network for Potential Intrusions]
[responsetemplate] Reply Here with the result CLEAN if clean[/responsetemplate] Multiverse Quantum State RGB CPU RAM ({quantum_state}).
"""

            llama_output = self.llm(automatic_prompt, max_tokens=459)
            print("Llama Output:", llama_output)  # Print llama output to terminal
            if 'choices' in llama_output and isinstance(llama_output['choices'], list) and len(llama_output['choices']) > 0:
                output_text = llama_output['choices'][0].get('text', '').strip()
                if output_text:
                    self.llama2_output_text.insert(tk.END, f"\nAI Response: {output_text}\n")
                    self.logger.info(f"Llama Output: {output_text}")
                    self.update_color_indicator(output_text)
                    retries -= 1  # Decrement retries only if a response is received
                    break  # Break out of the loop if a non-blank response is received
                else:
                    self.logger.warning("Llama output is blank.")
            else:
                self.logger.error("Llama output format is unexpected.")

            retries -= 1  # Decrement retries even if there's an error or no response
            if retries > 0:
                self.logger.info(f"Retrying... ({retries} retries left)")
                time.sleep(1)  # Wait for a short duration before retrying
            else:
                self.logger.error("Maximum retries reached without receiving a valid response.")
                break

    def update_color_indicator(self, output_text):
        normalized_output_text = output_text.lower()
        clean_regex = r'\b(clean)\b'
        dirty_regex = r'\b(dirty)\b'

        if re.search(clean_regex, normalized_output_text):
            color = self.calculate_color_code(80)  # Default color for clean response
            self.logger.info("Output is clean.")
        elif re.search(dirty_regex, normalized_output_text):
            color = self.calculate_color_code(20)  # Default color for dirty response
            self.logger.info("Output is dirty.")
        else:
            color = self.calculate_color_code(50)  # Default color for other responses
            self.logger.info("Output is neither clean nor dirty.")

        self.logger.info(f"Updating color indicator: {color}")
        self.color_indicator_canvas.delete("color_box")
        self.color_indicator_canvas.create_rectangle(0, 0, 40, 40, fill=color, tags="color_box")

    def setup_database(self):
        self.logger.info("Setting up database")
        self.loop = asyncio.get_event_loop()
        self.loop.run_until_complete(self.create_table())

    async def create_table(self):
        self.logger.info("Creating database table")
        async with aiosqlite.connect(self.db_name) as db:
            await db.execute("CREATE TABLE IF NOT EXISTS entries (id INTEGER PRIMARY KEY, data TEXT)")
            await db.commit()

    async def save_to_database(self, data):
        self.logger.info("Saving data to database")
        async with aiosqlite.connect(self.db_name) as db:
            await db.execute("INSERT INTO entries (data) VALUES (?)", (bleach.clean(self.fernet.encrypt(data.encode()).decode()),))
            await db.commit()

    async def retrieve_from_database(self):
        self.logger.info("Retrieving data from database")
        async with aiosqlite.connect(self.db_name) as db:
            cursor = await db.execute("SELECT data FROM entries")
            data = await cursor.fetchall()
            return [self.fernet.decrypt(d[0].encode()).decode() for d in data]

    def offline_quantum_circuit_analysis(self, ram_usage, cpu_usage):
        dev = qml.device("default.qubit", wires=3)
        qnode = self.create_quantum_circuit(dev)
        quantum_state = qnode(ram_usage, cpu_usage)
        print("Quantum State:", quantum_state)
        return quantum_state

    def create_quantum_circuit(self, dev):
        @qml.qnode(dev)
        def circuit(ram_usage, cpu_usage):
            r, g, b = self.ram_cpu_to_rgb(ram_usage, cpu_usage)
            qml.RY(np.pi * r / 255, wires=0)
            qml.RY(np.pi * g / 255, wires=1)
            qml.RY(np.pi * b / 255, wires=2)
            qml.CNOT(wires=[0, 1])
            qml.CNOT(wires=[1, 2])
            return qml.probs(wires=[0, 1, 2])

        return circuit

    def ram_cpu_to_rgb(self, ram_usage, cpu_usage):
        r = int(255 * (1 - np.exp(-ram_usage / 1024)))
        g = int(255 * (1 - np.exp(-ram_usage / 2048)))
        b = int(255 * (1 - np.exp(-cpu_usage / 100)))
        return r, g, b

    def get_ram_usage_without_internet(self):
        try:
            return psutil.virtual_memory().used
        except Exception as e:
            self.logger.error(f"Error getting RAM usage: {e}")
            raise

    def get_cpu_usage(self):
        try:
            return psutil.cpu_percent()
        except Exception as e:
            self.logger.error(f"Error getting CPU usage: {e}")
            raise

    def calculate_color_code(self, value):
        # Convert a numeric value to a unique color code
        hue = (value / 100) * 360  # Map value to hue in the range [0, 360]
        saturation = 0.8  # Adjust saturation for vibrant colors
        lightness = 0.5  # Medium lightness

        # Adjust the color gradient to produce a wider range of colors
        if 0 <= hue < 60:
            hue += 60  # Start with greenish colors
        elif 60 <= hue < 120:
            hue -= 60  # Move towards cyan
        elif 120 <= hue < 180:
            hue -= 120  # Move towards blue
        elif 180 <= hue < 240:
            hue -= 120  # Move towards purple
        elif 240 <= hue < 300:
            hue -= 180  # Move towards red
        else:
            hue -= 240  # Move towards orange/yellow

        color_code = self.hsl_to_hex(hue, saturation, lightness)
        return color_code

    def hsl_to_hex(self, h, s, l):
        # Convert hue, saturation, lightness to RGB color code
        h /= 360.0
        q = l * (1 + s) if l < 0.5 else l + s - l * s
        p = 2 * l - q
        r = self.hue_to_rgb(p, q, h + 1/3)
        g = self.hue_to_rgb(p, q, h)
        b = self.hue_to_rgb(p, q, h - 1/3)
        return "#{:02x}{:02x}{:02x}".format(int(r * 255), int(g * 255), int(b * 255))

    def hue_to_rgb(self, p, q, t):
        if t < 0:
            t += 1
        if t > 1:
            t -= 1
        if t < 1/6:
            return p + (q - p) * 6 * t
        if t < 1/2:
            return q
        if t < 2/3:
            return p + (q - p) * (2/3 - t) * 6
        return p

if __name__ == "__main__":
    app = App()